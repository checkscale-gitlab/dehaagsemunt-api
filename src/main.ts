import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe, HttpStatus, HttpException } from '@nestjs/common';
// import * as helmet from 'helmet';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { HttpExceptionFilter } from './modules/shared/filters/http-exception.filter';
// import { SecuritySchemeObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
// import { SnakeCaseInterceptor } from './shared/interceptors/snake-case.interceptor';
import * as Sentry from '@sentry/node';
import { SentryInterceptor } from './sentry.interceptor';

console.log('..main.ts');

async function bootstrap() {
  console.log('gooo bootstrap!!');
  const app = await NestFactory.create(AppModule);
  const logger = new Logger('Main', true);
  const globalPrefix = '/api';

  Sentry.init({
    dsn: AppModule.sentryKey,
    environment: process.env.NODE_ENV,
    release: 'dehaagsemunt-backend@' + process.env.npm_package_version,
  });

  app.useGlobalInterceptors(new SentryInterceptor());

  const a = new HttpException('Initial error 500', HttpStatus.INTERNAL_SERVER_ERROR);
  Sentry.captureException(a);

  app.enableCors();
  // app.use(helmet());
  app.setGlobalPrefix(globalPrefix);

  // Build the swagger doc only in dev mode
  if (AppModule.isDev) {
    const swaggerOptions = new DocumentBuilder()
      .setTitle('Driptop API')
      .setDescription('API documentation for Driptop')
      .addBearerAuth({ type: 'http', scheme: 'bearer', bearerFormat: 'JWT' })
      .build();

    const swaggerDoc = SwaggerModule.createDocument(app, swaggerOptions);

    SwaggerModule.setup(`${globalPrefix}/swagger`, app, swaggerDoc);
  }

  // Validate query params and body
  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  // Convert exceptions to JSON readable format
  app.useGlobalFilters(new HttpExceptionFilter());

  // Convert all JSON object keys to snake_case
  // app.useGlobalInterceptors(new SnakeCaseInterceptor());

  await app.listen(process.env.PORT);

  // Log current url of app
  let baseUrl = app.getHttpServer().address().address;
  if (baseUrl === '0.0.0.0' || baseUrl === '::') {
    baseUrl = 'localhost';
  }
  logger.log(`Listening to http://${baseUrl}:${AppModule.port}${globalPrefix}`);
  if (AppModule.isDev) {
    logger.log(`Swagger UI: http://${baseUrl}:${AppModule.port}${globalPrefix}/swagger`);
  }
}
bootstrap();
