import { Controller, Get, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { SentryInterceptor } from './sentry.interceptor';

@Controller()
export class AppController {
  @Get()
  @UseInterceptors(new SentryInterceptor())
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  home(): object {
    return {
      title: 'De Haagse Munt API',
      version: process.env.npm_package_version,
      environment: process.env.NODE_ENV,
    };
  }
}
