import { utilities as nestWinstonModuleUtilities, WinstonModule } from 'nest-winston';
import * as winston from 'winston';

export const logger = WinstonModule.createLogger({
    level: 'info', 
    format: winston.format.json(),
    transports: [
      new winston.transports.Console({
        format: winston.format.combine(
          winston.format.timestamp(),
          nestWinstonModuleUtilities.format.nestLike(),
        ),
      }),
      // - Write all logs with level `error` and below to `error.log`
      new winston.transports.File({ 
        filename: './ logs/error.log', 
        level: 'error' }),
      // - Write all logs with level `info` and below to `combined.log`
      new winston.transports.File({ filename: './logs/combined.log' }),
    ],
  })
  
  //
  // If we're not in production then log to the `console` with the format:
  // `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
  //
  // if (process.env.NODE_ENV !== 'production') {
  //   logger.add(new winston.transports.Console({
  //     format: winston.format.simple(),
  //   }));
  // }
