import { getRepository, MoreThan } from 'typeorm';
import { Logger, Injectable } from '@nestjs/common';
import { Transaction } from '../transaction/transaction.entity';
import { Voucher } from './voucher.entity';
import { User } from '../user/user.entity';
import { CreateTransactionDto } from '../transaction/transaction.interface';

@Injectable()
export class VoucherService {

  private readonly logger = new Logger(VoucherService.name); 

  constructor() {
    this.logger.log('Initiated');
  }

  async RedeemVoucher(sub: string, key: string): Promise<Voucher> {
    
    let voucher: Voucher;
    
    try {
      voucher = await getRepository(Voucher).findOne({
        where: [
          {
            key: key,
            redeemed: false,
            expires: MoreThan(new Date()),
          },
        ],
      });
    } catch (err) {
      throw Error('Error fetching voucher');
    }
    if (!voucher)  throw Error('Voucher not found');
    
    this.logger.log(`Voucher found: ${voucher.id}`);

    // Initiate new transaction
    const to = await getRepository(User).findOne({
      where: { auth0: sub },
    });
    if (!to) {
      throw new Error('User not found');
    }

    // Create instance
    const transactionData: CreateTransactionDto = {
      to: to,
      from: undefined,
      type: 'voucher',
      amount: voucher.amount,
      description: 'Voucher ' + voucher.id,
    };
    Logger.debug({
      message: 'voucher transactiondata created',
      data: transactionData,
    });

    Logger.debug('Creating new voucher db entity');
    const newTransaction = getRepository(Transaction).create({
      ...transactionData,
    });
    Logger.debug({ message: 'newTransactioncreated', data: newTransaction });

    // Save the transaction
    Logger.debug('Saving new transaction ');
    try {
      await getRepository(Transaction).save(newTransaction);
    } catch (err) {
      Logger.warn(err);
      throw new Error('Error saving new transaction');
    }

    Logger.debug('Setting voucher status to redeemed. ');
    try {
      voucher.redeemed = true;
      voucher.transaction = newTransaction;
      await getRepository(Voucher).save(voucher);
    } catch (err) {
      throw new Error('Voucher status updated');
    }

    return voucher;
  }
}
