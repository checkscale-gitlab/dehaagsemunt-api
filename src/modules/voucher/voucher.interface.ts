import { IsEmail, IsNumber, IsString, ValidateNested } from 'class-validator';

class UserInVoucherDto {
  @IsNumber()
  id!: number;
}

export class CreateVoucherDto {
  @IsNumber()
  amount!: number;

  @IsString()
  notes!: string;

  @IsEmail()
  @IsString()
  email!: string;

  @ValidateNested()
  user!: UserInVoucherDto;
}

export class RedeemVoucherDto {
  @IsString()
  key!: string;
}
