import * as crypto from 'crypto';
import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../user/user.entity';
import { Transaction } from '../transaction/transaction.entity';

@Entity()
export class Voucher {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ nullable: false })
  amount!: number;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  issued!: Date;

  @Column({ type: 'timestamp' })
  expires!: Date;

  @Column({ nullable: false, unique: true })
  key!: string;

  @Column({ nullable: false, default: 'dhm' })
  issuer!: string;

  @Column({ type: 'text' })
  notes!: string;

  @Column({ default: false })
  redeemed!: boolean;

  @Column({ nullable: true })
  email!: string;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt!: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt!: Date;

  // Associations
  @OneToOne(
    () => Transaction,
    transaction => transaction.voucher,
    {
      eager: true,
    },
  )
  @JoinColumn({ name: 'transaction_id' })
  transaction!: Transaction;

  @ManyToOne(
    () => User,
    user => user.vouchers,
    { eager: true },
  )
  @JoinColumn({ name: 'user_id' })
  user!: User;

  // Hooks
  @BeforeInsert()
  addKey() {
    this.key = crypto.randomBytes(6).toString('hex');
    const d = new Date();
    d.setFullYear(d.getFullYear() + 1);
    this.expires = d;
  }
}
