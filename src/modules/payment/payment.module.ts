import { Module } from '@nestjs/common';
import { PaymentController } from './payment.controller';
import { PaymentService } from './payment.service';
import { MollieService } from './mollie.service';

@Module({
  controllers: [PaymentController],
  providers: [PaymentService, MollieService],
  exports: [PaymentService],
})
export class PaymentModule {}
