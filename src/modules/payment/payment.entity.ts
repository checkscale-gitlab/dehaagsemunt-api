import { AfterUpdate, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
// import { User } from '../user/user.entity';
// import { Transaction } from '../transaction/transaction.entity';

@Entity()
export class Payment {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'text' })
  description!: string;

  @Column({ nullable: false })
  amount!: number;

  @Column({ nullable: false })
  costs!: number;

  get total(): number {
    return this.amount + this.costs;
  }

  @Column({ default: 'draft' })
  status!: string;

  @Column()
  mollie_id!: string;

  @Column({ type: 'text' })
  mollie_details!: string;

  @CreateDateColumn({ name: 'created_at', type: 'timestamp with time zone' })
  createdAt!: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp with time zone' })
  updatedAt!: Date;

  // // Associations
  // @OneToOne(_type => User)
  // @JoinColumn()
  // user: User;

  // @OneToOne(_type => Transaction)
  // @JoinColumn()
  // transaction: Transaction;

  // Listeners
  @AfterUpdate()
  createTransaction() {
    // Create the transaction after receiving a valid payment.
    // logger.debug('[ payment.model ] - Payment updated. Start creating the transaction if needed');
    // var oldStatus = payment._previousDataValues.status;
    // var newStatus = payment.dataValues.status;
    // logger.debug('[ payment.model ] - Old status: ' + oldStatus + ', new status: '+ newStatus);
    //
    // // Handle paid status change.
    // if ( (oldStatus!==newStatus) && (newStatus==='paid') ) {
    //
    //   models.user.findById(payment.dataValues.user_id).then(function(user) {
    //     if (!user) { logger.error('[ payment.model ] - No user found', payment.dataValues.user_id); }
    //
    //     // Send an email to user with payment received message
    //     mailService.send({
    //       msgType: 'paymentReceived',
    //       to: user.dataValues.email,
    //       amount: payment.dataValues.amount,
    //       total: payment.dataValues.total,
    //       substitutions: {
    //         username: user.dataValues.displayname
    //       }
    //     });
    //
    //     // Update activated status if the amount was > 10
    //     if ( (payment.dataValues.amount >= 10) && (user.dataValues.active == false) ) {
    //       logger.info('[ payment.model ] - Activating user ');
    //
    //       user.updateAttributes({
    //         active: true
    //       }).then(function(result) {
    //         logger.info('[ payment.model ] - Account updated to active');
    //       }).catch(function(err) {
    //         logger.error('[ payment.model ] - Error setting acount to active', err);
    //       });
    //     }
    //   }).catch(function(err) {
    //     logger.error(err);
    //   });
    //
    //   logger.debug('[ payment.model ] - Create the new transaction. ');
    //   // parse hm-amount in cents
    //   let hmAmount = parseInt(payment.dataValues.amount)*100;
    //   var t = {
    //     type_id:      1, // payment type number
    //     amount:       hmAmount,
    //     amountmeta:  '[' + hmAmount + ',0,0,0,0,0]',
    //     to_id:        payment.dataValues.user_id,
    //     description:  'Inleg door gebruiker. Payment nummer: ' + payment.dataValues.id
    //   };
    //   logger.debug('[ payment.model ] - Initiating new transaction, ', t);
    //   models.transaction.create(t).then(function(result) {
    //     if (!result) { logger.error('[ payment.model ] - No transaction created.'); }
    //     logger.debug('[ payment.model ] - Transaction created: ', result.dataValues.id);
    //     // Add the newly created transaction to the payment
    //     payment.setTransaction(result.dataValues.id).then(function(result) {
    //       logger.debug('[ payment.model ] - Transaction id added to payment ');
    //     }).catch(function(error) {
    //       logger.error('[ payment.model ] - Error', error);
    //     });
    //   }).catch(function(error) {
    //     logger.error('[ payment.model ] - Error', error);
    //   });
    // }
  }
}
