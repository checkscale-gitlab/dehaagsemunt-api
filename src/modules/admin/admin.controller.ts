import { Controller, Get, UseInterceptors, UseGuards } from '@nestjs/common';
import { SentryInterceptor } from './../../sentry.interceptor';
import { AuthGuard } from '@nestjs/passport';


@UseInterceptors(new SentryInterceptor)
@Controller('admin')
export class AdminController {
  
  @UseGuards(AuthGuard('jwt'))
  @Get()
  getStatus(): object {
    return {
      API: 'De Haagse Munt API',
      version: process.env.npm_package_version,
      endpoint: '/admin',
      protected: true
    };
  }
}
