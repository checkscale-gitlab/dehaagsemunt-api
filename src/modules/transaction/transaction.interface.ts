import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNumber, IsString, Length, Min, Max, ValidateNested } from 'class-validator';
import { TransactionType } from './transaction.entity';

export class UserInTransactionDto {
  @IsNumber()
  id!: number;
}

export class CreateTransactionDto {
  @ApiProperty({ description: 'The amount', minimum: 1, default: 1 })
  @IsNumber()
  @Min(1)
  @Max(10000)
  amount!: number;

  @ApiProperty()
  @IsString()
  @Length(3, 150)
  description?: string;

  @ApiProperty()
  @ValidateNested()
  from?: UserInTransactionDto;

  @ApiProperty()
  @ValidateNested()
  to!: UserInTransactionDto;

  @ApiProperty({ name: 'type', enum: TransactionType })
  @IsEnum(TransactionType)
  type!: string;
}

export class UpdateTransactionDto {
  @IsNumber()
  @Min(1)
  id!: number;

  @IsNumber()
  @Min(1)
  @Max(10000)
  amount!: number;

  @IsString()
  @Length(3, 150)
  description?: string;

  @ValidateNested()
  from?: UserInTransactionDto;

  @ValidateNested()
  to!: UserInTransactionDto;

  @IsEnum(TransactionType)
  type!: string;
}
