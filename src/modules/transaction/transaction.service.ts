import { getManager, getRepository } from 'typeorm';
// import { Queue } from 'bull';
// import { InjectQueue } from '@nestjs/bull';
import { CreateTransactionDto, UpdateTransactionDto } from './transaction.interface';
import { Transaction } from './transaction.entity';
import { Logger, Injectable } from '@nestjs/common';
import { BalanceService } from '../balance/balance.service';
import { ChangeBalanceInterface, CreateBalanceDto } from '../balance/balance.interface';
import { Balance } from '../balance/balance.entity';


class PagerOptions {
  take: number
  skip: number
}

@Injectable()
export class TransactionService {

  private logger = new Logger('TransactionService'); 

  constructor(
    private readonly balanceService: BalanceService,
    // @InjectQueue('transactions') private transactionQueue: Queue
  ) {
    this.logger.log('Initiating TransactionService')
  }

  /**
   *
   * Get a transaction 
   * 
   */
  async GetTransaction(sub: string, tid: string): Promise<Transaction | undefined> {
    return await getRepository(Transaction).findOne({
      where: { id: tid },
    });
  }

  /**
   *
   * Get a list of transactions
   * 
   */
  async GetTransactions(sub: string, pager: PagerOptions = null): Promise<Transaction[]> {
    if (!pager)  { 
      pager = { 
        take: 10, 
        skip:0
      }
    }

    const query = `
      SELECT 
        t.*, 
        b.amountmeta, 
        u.id as user_id,
        from_id.nickname as from_nickname,
        from_id.picture as from_picture,
        to_id.nickname as to_nickname,
        to_id.picture as to_picture
      FROM public.user u
      JOIN balance b
        on u.id = b.user_id
      JOIN transaction t ON
        b.transaction_id = t.id
      LEFT OUTER JOIN public.user from_id ON
        from_id.id = t.from_id
      LEFT OUTER JOIN public.user to_id ON
        to_id.id = t.to_id
      WHERE
        u.auth0 = $1
      AND
        ( t.from_id=u.id OR t.to_id=u.id)
      ORDER BY t.id DESC
      LIMIT ${pager.take}
      OFFSET ${pager.skip}`;

    return await getManager().query(query, [sub]);
  }

  /**
   *
   * Create a new transaction. Will be processed by the transaction worker service.
   *
   */
  async CreateTransaction(data: CreateTransactionDto): Promise<Transaction> {
    // Create the transaction database object
    const t = getRepository(Transaction).create(data);
    // Save the transaction to the database
    return await getRepository(Transaction).save(t);
  }

  /**
   *
   */
  async UpdateTransaction(transaction: UpdateTransactionDto): Promise<Transaction | undefined> {
    return await getRepository(Transaction).save(transaction);
  }

  /**
   *
   * Find the next oldest (next) transaction in need of processing
   *
   */
  async FindNextTransaction(): Promise<Transaction | undefined> {
    return await getRepository(Transaction).findOne({
      where: { processed: false },
      order: { id: 'ASC' },
    });
  }

  /**
   *
   * Process a transaction
   *
   * type <string> : transactiontype
   * amount: <number> : transaction amount
   * fromBalance: <Balance> : current balance of sender
   * toBalance: <Balance> : current balance of receiver
   * changeMeta: <iBalanceChange> : array of amountmeta changes
   *
   * calculateFundsAvailable():<boolean>
   * calculate new fromBalance: from: old: [], change: [], new: [] <ChangeBalanceInterface>
   * bump change_meta: []  <iAmountMeta>
   * calculate new toBalance : from: old: [], change: [], new: [] <ChangeBalanceInterface>
   *
   * Save newFromBalance
   * Save newToBalance
   * Update transaction status
   *
   * @param transaction
   *
   */
  async ProcessTransaction(transaction: Transaction): Promise<boolean | Error> {
    if (!transaction) {
      return Promise.reject(new Error('No transaction provided'));
    }

    const type: string = transaction.type;
    const amount: number = transaction.amount;
    let fromBalance: Balance | undefined;
    let toBalance: Balance;
    let newFromBalanceDto: CreateBalanceDto;
    // let newFromBalance: Balance | undefined;
    let bumpedMeta: number[] = [amount];
    // let newBalanceDto: CreateBalanceDto;
    // let newToBalance: Balance;

    // Get current balance for sender
    if (['transaction', 'withdrawl'].indexOf(type)) {
      try {
        fromBalance = await this.balanceService.CurrentBalance(String(transaction.from.auth0));
        if (!fromBalance) {
          return Promise.reject(new Error('No transaction provided'));
        }
      } catch (err) {
        return Promise.reject('No current from balance found');
      }
    }

    // Validate if sender has enough funds available
    if (fromBalance) {
      if (transaction.from && (fromBalance.amount || 0) < transaction.amount) {
        return Promise.reject(new Error('No sufficient funds for the transaction'));
      }
    }

    // Get current balance for recipient
    try {
      const b = await this.balanceService.CurrentBalance(String(transaction.to.auth0));
      if (!b) {
        return Promise.reject(new Error('No current to balance'));
      }
      toBalance = b;
    } catch (err) {
      return Promise.reject(err);
    }

    // Calculate new sender's balance
    if (fromBalance) {
      const b: ChangeBalanceInterface = this.balanceService.SubtractBalanceMeta(fromBalance.amountmeta, amount);
      newFromBalanceDto = {
        user: transaction.from,
        transaction: transaction,
        amountmeta: b.new,
      };
      // calculate bumped metadata
      bumpedMeta = this.balanceService.BumpMetaData(b.change);
      // save the new balance
      await this.balanceService.CreateBalance(newFromBalanceDto);
    }

    // calculate new recipient's balance
    const newToBalanceMeta: number[] = this.balanceService.AddBalanceMeta(toBalance.amountmeta, bumpedMeta);
    const newToBalanceDto: CreateBalanceDto = {
      user: transaction.to,
      amountmeta: newToBalanceMeta,
      transaction: transaction,
    };
    // save the new balance
    await this.balanceService.CreateBalance(newToBalanceDto);

    // Save the final transaction
    transaction.processed = true;
    transaction.status = true;
    await getRepository(Transaction).save(transaction);

    return true;
  }
}
