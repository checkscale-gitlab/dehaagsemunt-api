import { Controller, Get, Post, Body, Param, Req, UseGuards, UseInterceptors, HttpException, HttpStatus } from '@nestjs/common';
import { CreateTransactionDto } from './transaction.interface';
import { TransactionService } from './transaction.service';
import { AuthGuard } from '@nestjs/passport';
import { Transaction } from './transaction.entity';
import { SentryInterceptor } from './../../sentry.interceptor';
import { TransactionWorkerService } from './transaction.worker';

export interface RequestInterface extends Request {
  user: { sub: string }
  query?: { 
    take: number
    skip: number 
  }
}

@UseInterceptors(SentryInterceptor)
@Controller('transaction')
export class TransactionController {
  
  constructor(
    private readonly transactionService: TransactionService,
    private readonly transactionWorker: TransactionWorkerService
  ) {}

  /**
   * 
   * Create a new transaction for current user. 
   * 
   **/ 
  @UseGuards(AuthGuard('jwt'))
  @Post()
  async create(
    @Body() transactionDto: CreateTransactionDto, 
    @Req() req: RequestInterface): Promise<Transaction | HttpException> {
    
    const { user } = req; 
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);  

    let result: Transaction; 
    try {
      result = await this.transactionService.CreateTransaction(transactionDto);
    } catch(err) {
      return new HttpException('Error creating new transaction', HttpStatus.INTERNAL_SERVER_ERROR); 
    }
    
    // Notify the transactionworker to process the result
    this.transactionWorker.Ping();

    return result; 
  }

  /**
   * 
   * Get a list of transactions for current user. 
   * 
   **/ 
  @UseGuards(AuthGuard('jwt'))
  @Get()
  async findAll(@Req() req: RequestInterface): Promise<Transaction[] | HttpException> {
    
    const { user, query } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);  

    let result: Transaction[]; 
    const options = {
      take: Number(query.take) || 10,
      skip: Number(query.skip) || 0
    }

    try {
      result = await this.transactionService.GetTransactions(user.sub, options);
    } catch(err) {
      return new HttpException('Error fetching transactions list', HttpStatus.INTERNAL_SERVER_ERROR); 
    }
    
    return result; 
  }

  /**
   * 
   * Get a transaction for current user. 
   * 
   **/ 
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async findOne(@Req() req: RequestInterface, @Param('id') id: string): Promise<Transaction | HttpException> {
    
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);  

    let result: Transaction; 
    try {
      this.transactionService.GetTransaction(user.sub, id);
    } catch (err) {
      return new HttpException('Error fetching transactions list', HttpStatus.INTERNAL_SERVER_ERROR); 
    }

    return result; 
  }
}
