import { Injectable, Logger } from '@nestjs/common';
import { Transaction } from './transaction.entity';
import { TransactionService } from './transaction.service';

/***
 *
 * Ping()
 *  Start()
 *    While unprocessed transaction exists
 *      ProcessTransaction
 *
 *        type <string> : transactiontype
 *        amount: <number> : transaction amount
 *        fromBalance: <Balance> : current balance of sender
 *        toBalance: <Balance> : current balance of receiver
 *        changeMeta: <iBalanceChange> : array of amountmeta changes
 *
 *        calculateFundsAvailable():<boolean>
 *        calculate new fromBalance: from: old: [], change: [], new: [] <ChangeBalanceInterface>
 *        bump change_meta: []  <iAmountMeta>
 *        calculate new toBalance : from: old: [], change: [], new: [] <ChangeBalanceInterface>
 *
 *        Save newFromBalance
 *        Save newToBalance
 *        Update transaction status
 *
 */
@Injectable()
export class TransactionWorkerService {
  private readonly logger = new Logger(TransactionWorkerService.name);
  private running = false;

  constructor(
    private transactionService: TransactionService,
  ) {
    this.logger.log('init');
  }

  /**
   *
   * Ping and start the tranasction worker if needed
   *
   */
  async Ping(): Promise<boolean> {
    this.logger.log(`Ping transaction worker`);
    if (!this.running) await this.Start();
    
    this.logger.log(`Transaction worker return ping`);
    
    return true;
  }

  /**
   *
   * Start the transaction worker
   *
   */
  async Start(): Promise<boolean | undefined> {
    this.logger.log(`Start transaction worker`);
    // Set the transaction worker status to true
    this.running = true;

    // Find the next transaction, process it and re
    let transaction: Transaction | undefined;
    this.logger.log(`Fetching next transaction if available`);
    while ((transaction = await this.transactionService.FindNextTransaction()) !== undefined) {
      this.logger.log(`Transaction ${transaction.id} found, start processing`);
      try {
        await this.transactionService.ProcessTransaction(transaction);
      } catch (err) {
        await this.handleError(transaction, err);
      }
      this.logger.log(`Transaction ${transaction.id} finished processing`);
    }
    // No more transactions to process.
    this.logger.log(`Transaction worker done`);
    this.running = false;

    // Send the status back
    return !this.running;
  }

  /**
   *
   * 
   * 
   */
  private async handleError(transaction: Transaction, error: Error): Promise<boolean> {
    transaction.processed = true;
    transaction.status = false;
    // eslint-disable-next-line @typescript-eslint/camelcase
    transaction.status_msg = error.message;

    try {
      await this.transactionService.UpdateTransaction(transaction);
    } catch (err) {
      this.logger.error(err);
    }

    return false;
  }
}
