import { Controller, Get, Req, UseGuards, UseInterceptors, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { BalanceService } from './balance.service';
import { Balance } from './balance.entity';
import { AuthGuard } from '@nestjs/passport';
import { SentryInterceptor } from './../../sentry.interceptor';

export interface RequestInterface extends Request {
  user: {
    sub: string;
  };
}

@UseInterceptors(SentryInterceptor)
@Controller('balance')
export class BalanceController {

  private logger = new Logger(BalanceController.name); 

  constructor(
    private readonly balanceService: BalanceService
  ) {
    this.logger.log('Initiating BalanceController')
  }

  /**
   *
   * @param req
   */
  @UseGuards(AuthGuard('jwt'))
  @Get()
  async getBalanceList(@Req() req: RequestInterface): Promise<Balance[] | HttpException> {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST); 
    
    let result: Balance[]; 
    try {
      result = await this.balanceService.BalanceList(user.sub);
    } catch(err) {
      return new HttpException('error fetching balance list', HttpStatus.INTERNAL_SERVER_ERROR); 
    }

    return result; 
  }

  /**
   *
   * @param req
   */
  @UseGuards(AuthGuard('jwt'))
  @Get('/current')
  async getBalance(@Req() req: RequestInterface): Promise<Balance | HttpException> {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST); 

    let result: Balance;
    try {
      result = await this.balanceService.CurrentBalance(user.sub);
    } catch(err) {
      return new HttpException('error fetching balance list', HttpStatus.INTERNAL_SERVER_ERROR); 
    }

    return result;
  }
}
