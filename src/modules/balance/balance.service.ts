import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import { Balance } from './balance.entity';
import { ChangeBalanceInterface, CreateBalanceDto } from './balance.interface';
// import { Transaction } from '../transaction/transaction.entity';

@Injectable()
export class BalanceService {
  /**
   *
   * Create a new balance
   *
   * @param balanceDto
   *
   */
  async CreateBalance(balanceDto: CreateBalanceDto): Promise<Balance> {
    const b = getRepository(Balance).create({ ...balanceDto });
    return await getRepository(Balance).save(b);
  }

  /**
   *
   * Get current balance for a user
   *
   * @param sub
   *
   */
  async CurrentBalance(sub: string): Promise<Balance | undefined> {
    return await getRepository(Balance)
      .createQueryBuilder('balance')
      .leftJoin('balance.user', 'user')
      .where('user.auth0=:sub', { sub: sub })
      .orderBy('balance.id', 'DESC')
      .getOne();
  }

  /**
   *
   * Get a list of recent balances for a user
   *
   * @param sub
   *
   */
  async BalanceList(sub: string): Promise<Balance[] | null> {
    return await getRepository(Balance)
      .createQueryBuilder('balance')
      .leftJoin('balance.user', 'user')
      .leftJoinAndSelect('balance.transaction', 'transaction')
      .leftJoinAndSelect('transaction.from', 'from')
      .leftJoinAndSelect('transaction.to', 'to')
      .where('user.auth0 = :sub', { sub: sub })
      .orderBy('balance.id', 'DESC')
      .getMany();
  }

  /**
   *
   * Subtract an amount from a balance
   *
   * @param balanceMeta
   * @param amount
   */
  SubtractBalanceMeta(_meta: number[], _amount: number): ChangeBalanceInterface {
    let oldMeta: number[] = _meta;
    let changeMeta: number[] = [];
    let newMeta: number[] = [];
    let amountLeft = _amount;

    oldMeta = oldMeta.reverse();
    oldMeta.map((stepValue: number, index: number) => {
      const amountToSubtract = Math.min(amountLeft, stepValue);
      changeMeta[index] = amountToSubtract;
      newMeta[index] = oldMeta[index] - amountToSubtract;
      amountLeft = amountLeft - amountToSubtract;
    });

    oldMeta = oldMeta.reverse();
    changeMeta = changeMeta.reverse();
    newMeta = newMeta.reverse();

    return {
      old: oldMeta,
      change: changeMeta,
      new: newMeta,
    };
  }

  /**
   * @param metaData
   */
  BumpMetaData(metaData: number[]): number[] {
    metaData.unshift(0);
    metaData[metaData.length] += metaData[metaData.length + 1];
    metaData.splice(metaData.length, 1);

    return metaData;
  }

  /**
   *
   * @param meta
   * @param change
   */
  AddBalanceMeta(meta: number[], change: number[]): number[] {
    meta.map((item: number, index: number) => {
      meta[index] = (meta[index] || 0) + (change[index] || 0);
    });

    return meta;
  }
}
