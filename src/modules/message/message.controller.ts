import { Controller, Get, Put, Req, UseGuards, Param, Body, UseInterceptors, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { MessageService } from './message.service';
import { AuthGuard } from '@nestjs/passport';
import { UpdateMessageDto } from './message.interface';
import { Message } from './message.entity';
import { SentryInterceptor } from './../../sentry.interceptor';
import { UpdateResult } from 'typeorm';

export interface RequestInterface extends Request {
  user: any;
}

@UseInterceptors(new SentryInterceptor)
@Controller('message')
export class MessageController {

  private readonly logger = new Logger(MessageController.name); 

  constructor(
    private readonly messageService: MessageService
  ) {
   this.logger.log('Initiated') 
  }

  /**
   * 
   * Find all messages for current user. 
   * 
   **/ 
  @UseGuards(AuthGuard('jwt'))
  @Get()
  async findAll(@Req() req: RequestInterface): Promise<Message[] | HttpException> {
    
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST); 

    let result: Message[]; 
    try {
      result = await this.messageService.getMessages({ sub: user.sub });
    } catch(err) {
      return new HttpException('Error fetching message list', HttpStatus.INTERNAL_SERVER_ERROR); 
    }

    return result; 
  }

  /**
   * 
   * Get the number of unread messages for a user. 
   * 
   **/ 
  @UseGuards(AuthGuard('jwt'))
  @Get('unread')
  async getUnreadCount(@Req() req: RequestInterface): Promise<number | HttpException> {
    
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST); 

    let result: number; 
    try {
      result = await this.messageService.getUnreadCount(user.sub);
    } catch (err) {
      return new HttpException('Error fetching unread message count', HttpStatus.INTERNAL_SERVER_ERROR); 
    }

    return result; 
  }

  /**
   * 
   * Get a single message for current user. 
   * 
   **/ 
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getMessage(
    @Param('id') id: string,
    @Req() req: RequestInterface
  ): Promise<Message | HttpException> {
    
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST); 

    let result: Message; 
    try {
      result = await this.messageService.getMessage({ sub: user.subm, messageId: Number(id) });
    } catch(err) {
      return new HttpException(`Error fetching message ${id}`, HttpStatus.INTERNAL_SERVER_ERROR); 
    }

    return result; 
  }

  /**
   * 
   * Update a single message for current user. 
   * 
   **/ 
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateMessage(
    @Param('id') id: number,
    @Req() req: RequestInterface,
    @Body() updateData: UpdateMessageDto,
  ): Promise<UpdateResult | HttpException> {

    // Make sure current user exists 
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST); 

    let result: UpdateResult; 
    try {
      result = await this.messageService.UpdateMessage(user.sub, id, updateData);
    } catch(err) {
      return new HttpException(`Error fetching message ${id}`, HttpStatus.INTERNAL_SERVER_ERROR); 
    }

    return result; 
  }
}
