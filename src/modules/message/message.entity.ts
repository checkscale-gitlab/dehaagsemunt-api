import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../user/user.entity';

export enum MessageLevel {
  SEVERE = 'severe',
  IMPORTANT = 'important',
  INFO = 'info',
}

@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  readonly id!: number;

  @Column({
    nullable: false,
  })
  subject!: string;

  @Column({
    type: 'text',
    nullable: false,
  })
  body!: string;

  @Column()
  type!: string;

  @Column({
    name: 'level',
    type: 'enum',
    enum: MessageLevel,
    default: MessageLevel.INFO,
  })
  level!: MessageLevel;

  @Column({
    default: false,
  })
  emailed_status!: boolean;

  @Column({
    default: false,
  })
  read_status!: boolean;

  @Column({
    default: false,
  })
  delete_status!: boolean;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz',
    readonly: true,
  })
  createdAt!: Date;

  @ManyToOne(
    () => User,
    user => user.messages,
  )
  @JoinColumn({
    name: 'user_id',
  })
  user!: User;
}
