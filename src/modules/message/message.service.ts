import { getConnection, getRepository, Brackets, UpdateResult } from 'typeorm';
import { Message } from '../message/message.entity';
import { CreateMessageDto, UpdateMessageDto } from '../message/message.interface';
import { User } from '../user/user.entity';
import { Logger, Injectable } from '@nestjs/common';
import { IMessageQueryOptions } from './message.interface';

@Injectable()
export class MessageService {
  private logger = new Logger(MessageService.name); 

  constructor() {
    this.logger.log('initiated')
  }
  
  // Fetch messages for a user
  async getMessages(opts: IMessageQueryOptions): Promise<Message[] | null> {
    const { sub } = opts;
    if (!sub) {
      Logger.warn('[ message.service ] - No user provided. ');
    }

    try {
      const query = getRepository(Message)
        .createQueryBuilder('message')
        .innerJoin(User, 'user', 'user.id = message.user_id')
        .select(['user.id'])
        .addSelect('message')
        .where('user.auth0 = :sub', { sub })
        .orderBy('message.id', 'DESC')
        .take(100);

      if (opts.read_status) {
        query.andWhere('message.read_status = :rs', { rs: opts.read_status });
      }
      if (opts.delete_status) {
        query.andWhere('message.delete_status = :ds', {
          ds: opts.delete_status,
        });
      }

      const messages: Message[] = await query.getMany();

      return messages;
    } catch (err) {
      Logger.warn('getMessages error', err);
      return null;
    }
  }

  // Fetch message for a user
  async getMessage(opts: IMessageQueryOptions): Promise<Message | null> {
    if (!opts.sub) {
      Logger.warn('[ message.service ] - No user provided. ');
    }
    if (!opts.messageId) {
      Logger.warn('[ message.service ] - No messageid provided. ');
    }

    try {
      const query = getRepository(Message)
        .createQueryBuilder('message')
        .innerJoin(User, 'user', 'user.id = message.user_id')
        .select(['user.id'])
        .addSelect('message')
        .where('user.auth0 = :sub', { sub: opts.sub })
        .andWhere('message.id = :messageId', { messageId: opts.messageId });

      const message: any = await query.getOne();

      return message;
    } catch (err) {
      Logger.warn('getMessage err', err);
      return null;
    }
  }

  /**
   *
   * Update the details of a message
   *
   * @param mid
   * @param uid
   * @param updateData
   *
   */
  async UpdateMessage(sub: string, mid: number, updateData: UpdateMessageDto): Promise<UpdateResult> {

    this.logger.log(`Updating message ${mid} for user ${sub} with values ${JSON.stringify(updateData)}`)
    
    return await getRepository(Message)
      .createQueryBuilder('message')
      .innerJoin(User, 'user', 'user.id = message.user_id')
      .select(['user.id'])
      .addSelect('message')
      .update(Message)
      .set(updateData)
      .where('user.auth0 = :sub')
      .andWhere('message.id = :mid', { mid })
      .execute();

  }

  /**
   *
   * @param offset
   */
  async GetUnsentMessagesByUser(offset: number | null = null): Promise<Message | null> {
    // Set selection date
    offset = offset || 24 * 60 * 60 * 1000 * 1; //1 day;
    const d = new Date();
    d.setTime(d.getTime() - offset);
    d.toISOString();
    // Set query params
    const filters: any = {
      digestDate: d as any,
    };

    // Find all unread messages and their users.
    let m: any;
    try {
      m = await getConnection()
        .createQueryBuilder()
        .select('COUNT(*) AS message_count')
        .from(Message, 'message')
        .leftJoin('message.user', 'user')
        .addSelect(['user.id', 'user.auth0', 'user.email', 'user.nickname', 'user.last_email_digest'])
        .where('message.emailed_status = :status', { status: false })
        .andWhere(
          new Brackets(qb => {
            qb
              .where('user.last_email_digest < :digestDate')
              .orWhere('user.last_email_digest IS NULL');
          }),
        )
        .setParameters(filters)
        .groupBy('user.id')
        .getRawMany();
    } catch (err) {
      return Promise.reject(err);
    }

    return m;
  }

  /**
   *
   * @param sub
   */
  async getUnreadCount(sub: string): Promise<any> {
    return await getRepository(Message)
      .createQueryBuilder('message')
      .select('COUNT(message.read_status)', 'count')
      .innerJoin(User, 'user', 'user.id = message.user_id')
      .where('message.read_status = false')
      .andWhere('message.delete_status = false')
      .andWhere('user.auth0 = :sub', { sub })
      .getRawOne();
  }

  /**
   *
   * @param obj
   */
  async createMessage(msgDto: CreateMessageDto): Promise<Message | null> {
    const msg: Message = getRepository(Message).create(msgDto);
    try {
      await getRepository(Message).save(msg);
      return msg;
    } catch (err) {
      Logger.warn('createMessage err', err);
      return null;
    }
  }
}
