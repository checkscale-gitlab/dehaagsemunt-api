import { User } from '../user/user.entity';
import { MessageLevel } from './message.entity';

export class CreateMessageDto {
  user!: User;

  subject!: string;

  body!: string;

  type!: string;

  level?: MessageLevel;

  emailed_status?: boolean;

  read_status?: boolean;

  delete_status?: boolean;
}

export class UpdateMessageDto {
  subject?: string;
  body?: string;
  type?: string;
  level?: MessageLevel;
  emailed_status?: boolean;
  read_status?: boolean;
  delete_status?: boolean;
}

export class IMessageQueryOptions {
  user?: User;
  sub?: string;
  messageId?: number;
  limit?: number;
  order?: number;
  read_status?: boolean;
  delete_status?: boolean;
}

export class IMessageCount {
  user_id?: number;
  user_auth0?: string;
  user_email?: string;
  user_nickname?: string;
  user_last_email_digest?: string;
  message_count?: number;
}
