import { Controller, Get, Logger, UseGuards, Req, HttpException, HttpStatus, UseInterceptors } from '@nestjs/common';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';
import { BalanceService } from '../balance/balance.service';
import { TransactionService } from '../transaction/transaction.service';
import { User } from './user.entity';
import { SentryInterceptor } from './../../sentry.interceptor';
import { CurrentUserDto } from './user.interface';

export interface RequestInterface extends Request {
  user: { sub: string }
  query?: { nickname: string }
}

@UseInterceptors(new SentryInterceptor)
@Controller('user')
export class UserController {
  
  private logger = new Logger('UserController'); 

  constructor(
    private readonly userService: UserService,
    private readonly balanceService: BalanceService,
    private readonly transactionService: TransactionService,
  ) {
    this.logger.log('Initiating UserController. '); 
  }

  /**
   * 
   * Get current user's details
   * 
   **/ 
  @UseGuards(AuthGuard('jwt'))
  @Get('me')
  async getCurrentUser(@Req() request: RequestInterface): Promise<CurrentUserDto | HttpException> {
    
    const { user } = request;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result: CurrentUserDto; 

    try {
      const account = await this.userService.GetUser(user.sub);
      if (!account) return new HttpException('User not found', HttpStatus.NOT_FOUND);
      const balance = await this.balanceService.CurrentBalance(user.sub);
      const transactions = await this.transactionService.GetTransactions(user.sub);

      result = { account, balance, transactions };
    } catch (err) {
      Logger.warn(err);
      throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result; 
  }

  /**
   * 
   * Search a user 
   * 
   **/ 
  @UseGuards(AuthGuard('jwt'))
  @Get('search')
  async searchUser(@Req() req: RequestInterface): Promise<User[] | HttpException> {
    
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    const {query} = req; 
    if (!query || !query.nickname) return new HttpException('No nickname povided', HttpStatus.BAD_REQUEST);
    
    let result: User[]; 
    try {
      result = await this.userService.SearchUser(query.nickname, user.sub);
    } catch(err) {
      Logger.warn(err);
      throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result; 
  }
}
