import { IsBoolean, IsDate, IsEmail, IsOptional, IsString, Length, MaxLength, IsNumber, IsPositive } from 'class-validator';
import { Escape, Trim } from 'class-sanitizer';
import { Expose } from 'class-transformer';
import { User } from './user.entity';
import { Balance } from '../balance/balance.entity';
import { Transaction } from '../transaction/transaction.entity';

export class CreateUserDto {
  @Expose()
  @IsString()
  readonly auth0?: string;

  @Expose()
  @IsEmail()
  readonly email?: string;

  @Expose()
  @IsBoolean()
  public email_verified?: boolean;

  @Expose()
  @IsString()
  @Length(3, 25)
  @Escape()
  @Trim()
  public nickname?: string;

  @Expose()
  @IsString()
  @Length(3, 25)
  @Escape()
  @Trim()
  public slug?: string;

  @Expose()
  @IsString()
  @Escape()
  @Trim()
  public picture?: string;

  @Expose()
  @IsString()
  public account_plan?: string;

  @Expose()
  @IsDate()
  @IsOptional()
  public account_plan_end?: Date;

  @Expose()
  @IsBoolean()
  public admin?: boolean;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(15)
  @Escape()
  @Trim()
  public firstname?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(20)
  @Escape()
  @Trim()
  public lastname?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(300)
  @Escape()
  @Trim()
  public bio?: string;

  @Expose()
  @IsOptional()
  @IsDate()
  public signed_up!: Date;

  @Expose()
  @IsOptional()
  @IsNumber()
  @IsPositive()
  notification_schedule?: number;
}

export class UpdateUserDto {
  @Expose()
  @IsOptional()
  @IsBoolean()
  email_verified?: boolean;

  @Expose()
  @IsOptional()
  @IsString()
  @Length(3, 25)
  @Escape()
  @Trim()
  nickname?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @Escape()
  @Trim()
  picture?: string;

  @Expose()
  @IsOptional()
  @IsString()
  account_plan?: string;

  @Expose()
  @IsOptional()
  @IsDate()
  @IsOptional()
  account_plan_end?: Date;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(15)
  @Escape()
  @Trim()
  firstname?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(20)
  @Escape()
  @Trim()
  lastname?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(300)
  @Trim()
  @Escape()
  bio?: string;

  @Expose()
  @IsOptional()
  @IsDate()
  signed_up?: Date;

  @Expose()
  @IsOptional()
  @IsDate()
  last_email_notification?: Date;

  @Expose()
  @IsOptional()
  @IsNumber()
  @IsPositive()
  notification_schedule?: number;
}

export class CurrentUserDto {
  account: User
  balance: Balance
  transactions: Transaction[]
}