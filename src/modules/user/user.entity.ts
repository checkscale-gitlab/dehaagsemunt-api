import { Entity, Column, JoinTable, OneToMany, PrimaryGeneratedColumn, CreateDateColumn, ManyToMany } from 'typeorm';
import { Balance } from '../balance/balance.entity';
import { Message } from '../message/message.entity';
import { Voucher } from '../voucher/voucher.entity';
import { Transaction } from '../transaction/transaction.entity';

const DAILY = (60*24);
// const WEEKLY = (60*24*7);
// const MONTHLY = (60*24*30);

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  readonly id!: number;

  @Column({ unique: true })
  auth0?: string;

  @Column({
    unique: true,
    select: false,
  })
  email!: string;

  @Column({ select: false })
  email_verified!: boolean;

  @Column({ unique: true })
  nickname!: string;

  @Column({ unique: true })
  slug!: string;

  @Column()
  picture!: string;

  @Column()
  account_plan!: string;

  @Column({
    type: 'timestamptz',
    nullable: true,
  })
  account_plan_end!: Date;

  @Column({ default: false })
  admin!: boolean;

  @Column({ type: 'text', nullable: true })
  bio!: string;

  @Column({ nullable: true })
  firstname!: string;

  @Column({ nullable: true })
  lastname!: string;

  @Column({
    type: 'timestamptz',
    nullable: true,
  })
  signed_up!: Date;

  @Column({ nullable: true, default: DAILY })
  notification_schedule!: number;

  @Column({ nullable: true })
  last_email_notification!: Date;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz',
  })
  createdAt!: Date;

  @ManyToMany(() => User)
  @JoinTable()
  contacts!: User[];

  @OneToMany(
    () => Balance,
    balance => balance.user,
  )
  balances!: Balance[];

  @OneToMany(
    () => Transaction,
    transaction => transaction.to,
  )
  transactions!: Transaction[];

  @OneToMany(
    () => Message,
    message => message.user,
  )
  messages!: Message[];

  @OneToMany(
    () => Voucher,
    voucher => voucher.user,
  )
  vouchers!: Voucher[];
}
