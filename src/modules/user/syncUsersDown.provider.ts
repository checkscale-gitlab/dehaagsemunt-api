/**
 *
 * Provider to sync users from auth0 to the local db.
 * This will run at startup and block the initialization
 *
 * https://docs.nestjs.com/fundamentals/custom-providers#factory-providers-usefactory
 *
 */

import { UserService } from '../user/user.service';

export const SyncUsersDownProvider = {
  provide: 'FETCH_AUTH0_USERS',

  useFactory: async (userService: UserService): Promise<boolean> => {
    return await userService.SyncUSersFromAuth0();
  },
  inject: [UserService],
};
