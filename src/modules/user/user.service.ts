/* eslint-disable @typescript-eslint/camelcase */
import { Injectable, Logger } from '@nestjs/common';
import { getRepository, getConnection, Like, Not } from 'typeorm';
import { validate } from 'class-validator';
import * as moment from 'moment';
import * as nodeSlug from 'slug';
import { User } from './user.entity';
import { Auth0Service } from './../auth0/auth0.service';
import { CreateUserDto, UpdateUserDto } from './user.interface';
import * as Auth0 from 'auth0';
import { Balance } from '../balance/balance.entity';

@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);

  constructor(private readonly auth0Service: Auth0Service) {
    this.logger.log('init');
  }

  /**
   *
   * Find user and fetch from auth0 if not exists
   *
   **/
  GetUser = async (sub: string): Promise<User | undefined> => {
    let user: User | undefined;

    try {
      user = await getRepository(User).findOne({ where: { auth0: sub } });
    } catch (err) {
      Logger.warn(err);
      return err;
    }

    return user;
  };

  /**
   *
   * Create new user in the local db
   *
   */
  CreateUser = async (newUser: CreateUserDto): Promise<User> => {
    const errors = await validate(newUser);
    if (errors.length) {
      // console.log(errors);
      return Promise.reject(errors);
    }

    let user: User;
    try {
      user = getRepository(User).create(newUser);
      await getRepository(User).save(user);

      // Create initial balance
      await getRepository(Balance).save({ user, amountmeta: [0] });
    } catch (err) {
      return Promise.reject(err);
    }

    return user;
  };

  /**
   *
   */
  UpdateUser = async (sub: string, updatedUser: UpdateUserDto): Promise<boolean> => {
    const errors = await validate(updatedUser);
    if (errors.length) return Promise.reject(errors);

    try {
      await getConnection()
        .createQueryBuilder()
        .select('user')
        .from(User, 'user')
        .update(updatedUser)
        .where({ auth0: sub })
        .execute();
    } catch (err) {
      return Promise.reject(err);
    }

    return true;
  };

  /**
   *
   * Get all users stored in Auth0 (user database)
   * and synchronize to local db
   *
   */
  SyncUSersFromAuth0 = async (): Promise<boolean> => {
    this.logger.log(`SyncUSersFromAuth0 - init`);
    // Fetch users from auth0
    const users = await this.auth0Service.GetAuth0Users();
    this.logger.log(`SyncUSersFromAuth0 - users found: ${users.length}`);
    // Process each user
    for await (const user of users) {
      this.logger.log(`SyncUSersFromAuth0 - processing user: ${user.user_id}`);
      try {
        // Create user object from auth0
        const newUser = await this.CastAuth0ToUser(user);
        // console.log(newUser)
        if (!newUser) return false;
        const existingUser = await this.GetUser(newUser.auth0);

        if (existingUser) {
          // console.log('existing user', existingUser);
          await this.UpdateUser(String(newUser.auth0), newUser);
          Logger.log('user updated', newUser.email);
        } else {
          console.log('new user', existingUser, newUser);
          await this.CreateUser(newUser);
          // console.log('user created', newUser.email);
        }
      } catch (err) {
        console.error(err);
      }
    }

    // this.logger.log('SyncUSersFromAuth0 - done');
    return true;
  };

  /**
   *
   * Convert Auth0 user to local db user object
   *
   **/
  async CastAuth0ToUser(auth0user: Auth0.User): Promise<CreateUserDto | null> {
    // logger.debug(`[ user.service ] CastAuth0ToUser - Initiating user mapping: ${auth0user.email} `);
    if (!auth0user) return null;

    const { email, nickname, user_id, app_metadata, user_metadata } = auth0user;
    const { slug, account_plan } = app_metadata;
    const { notification_schedule } = user_metadata;

    let newNickname = nickname;
    let newSlug = slug;
    let newAccountPlan = account_plan;

    if (!user_id) return null;

    // Make sure there is a nickname
    if (!nickname) {
      this.logger.log(`CastAuth0ToUser - Nickname is not present, creating from email address.`);
      const nick = email.split('@')[0];
      this.logger.log(`CastAuth0ToUser - New nickname created: ${nick}`);

      newNickname = await this.auth0Service.GenerateUniqueNickname(nick);
      this.logger.log(`CastAuth0ToUser - New unique nickname created: ${newNickname}`);

      this.logger.log(`CastAuth0ToUser - Save new nickname to auth0`);
      await this.auth0Service.UpdateAuth0User(user_id, {
        nickname: newNickname,
      });
    }

    // make sure the user has a slug
    if (!slug) {
      newSlug = nodeSlug(auth0user.nickname);
      this.logger.log(`CastAuth0ToUser - Add new slug: ${newSlug}`);
      await this.auth0Service.UpdateAuth0User(user_id, {
        app_metadata: { slug: newSlug },
      });
    }

    // make sure the user has an account plan
    if (!account_plan) {
      newAccountPlan = 'basic';
      await this.auth0Service.UpdateAuth0User(user_id, {
        app_metadata: { account_plan: newAccountPlan },
      });
    }

    // make sure the user has a notification schedule
    if (!Number(notification_schedule)) {
      await this.auth0Service.UpdateAuth0User(user_id, {
        user_metadata: { notification_schedule: 60 * 24 },
      });
    }

    // Create the new user object.
    const newUser: CreateUserDto = {
      auth0: auth0user.user_id,
      email: auth0user.email,
      email_verified: auth0user.email_verified,
      nickname: newNickname,
      slug: newSlug,
      firstname: user_metadata.firstname,
      lastname: user_metadata.lastname,
      bio: user_metadata.bio,
      picture: auth0user.picture,
      account_plan: newAccountPlan,
      account_plan_end: app_metadata.account_plan_end,
      admin: (app_metadata.roles && app_metadata.roles.indexOf('admin') !== -1) || false,
      signed_up: moment(auth0user.created_at).toDate(),
      notification_schedule: Number(notification_schedule) || 60 * 24,
    };

    // logger.debug(`[ user.service ] CastAuth0ToUser - New user DTO created`);
    // console.log(newUser)
    return newUser;
  }

  /**
   *
   * @param query
   * @param sub
   */
  async SearchUser(query: string, sub: string): Promise<User[] | null> {
    Logger.log(`[ account.controller search ] - Query for ${query} `);
    // Validation
    const nickname = query;
    if (!nickname) {
      // return next(new HttpException(404, 'No username provided'));
      return null;
    }
    // if (nickname.length<3) {
    //   return next(new HttpException(403, 'Input too short'));
    // }

    // Query for users
    let users: User[] = [];
    try {
      users = await getRepository(User).find({
        where: {
          nickname: Like(`%${nickname}%`),
          auth0: Not(sub),
        },
        select: ['nickname', 'picture', 'auth0'],
        take: 20,
      });
    } catch (err) {
      // return next(new HttpException(500, err));
    }

    return users;
  }
}
