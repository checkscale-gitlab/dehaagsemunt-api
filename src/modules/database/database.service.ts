import { Inject, Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';

@Injectable()
export class DatabaseService {
  constructor(
    @Inject('Connection')
    public connection: Connection,
  ) {}

  async dropDatabase() {
    return await this.connection.dropDatabase();
  }

  async synchronizeDatabase() {
    return await this.connection.synchronize();
  }

  async runMigrations() {
    return await this.connection.runMigrations();
  }

  async getRepository<T>(entity: any): Promise<Repository<T>> {
    return this.connection.getRepository(entity);
  }
}
