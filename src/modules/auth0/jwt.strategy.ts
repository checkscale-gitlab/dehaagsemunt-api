import { passportJwtSecret } from 'jwks-rsa';
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { Injectable, UnauthorizedException, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(readonly configService: ConfigService) {
    super({
      secretOrKeyProvider: passportJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: configService.get('AUTH0_JWKSURI'),
      }),

      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), // 1

      audience: configService.get('AUTH0_AUDIENCE'),

      issuer: `https://${configService.get('AUTH0_DOMAIN')}/`,
    });
  }

  validate(payload: any, done: VerifiedCallback) {
    Logger.log('payload', payload);
    if (!payload) {
      done(new UnauthorizedException(), false); // 2
    }

    return done(null, payload);
  }
}
