export class Auth0UserDto {
  user_id!: string;
  email?: string;
  nickname?: string;
  picture?: string;
  created_at?: Date;
  email_verified?: boolean;
  app_metadata?: {
    slug?: string;
    account_plan?: string;
    account_plan_end?: Date;
    last_email_notification?: Date;
    admin?: boolean;
    roles?: Array<string>;
  };
  user_metadata?: {
    firstname?: string;
    lastname?: string;
    bio?: string;
    notification_schedule?: number;
  };
}

export class UpdateAuth0UserDto {
  email?: string;
  nickname?: string;
  picture?: string;
  created_at?: Date;
  email_verified?: boolean;
  app_metadata?: {
    slug?: string;
    account_plan?: string;
    account_plan_end?: Date;
    last_email_notification?: Date;
    admin?: boolean;
    roles?: Array<string>;
  };
  user_metadata?: {
    firstname?: string;
    lastname?: string;
    bio?: string;
    notification_schedule?: number;
  };
}
