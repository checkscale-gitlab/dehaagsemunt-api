/**
 *
 * Send an update with last month activity and current status.
 *
 **/

import { Logger, InternalServerErrorException } from '@nestjs/common';
import { getRepository, Between, 
  LessThanOrEqual
 } from 'typeorm';
import { User } from 'src/modules/user/user.entity';
import { Transaction} from '../../transaction/transaction.entity';
import moment = require('moment');
import { BalanceService } from '../../balance/balance.service';
import { Balance } from 'src/modules/balance/balance.entity';
import { EmailService } from 'src/modules/email/email.service';

const balanceService = new BalanceService();
const emailService = new EmailService(); 

const AccountStatusCron = async (): Promise<boolean> => {
  const logger = new Logger('monthly_account_status');
  logger.log('init');

  /**
   * Get all active users with: 
   *  - current balance
   *  - balance last month 
   *  - number of transactions last month 
   * 
   * **/
  try {
    const users = await getRepository(User).find({
      select: ["id", "email", "nickname", "auth0"]
    }); 

    // process each user 
    users.map(async user => {
      const now = moment().toDate();
      const lastMonth = moment().subtract(1, 'month').toDate();

      try {  
        // Get current balance 
        const currentBalance = await balanceService.CurrentBalance(user.auth0); 
      
        // Get final balance of last month 
        const previousBalance = await getRepository(Balance).findOne({
          where: {
            user: user,
            createdAt: LessThanOrEqual(lastMonth)
          },
          order: {createdAt: 'DESC'}
        })

        // Get number of transactions in last month 
        const transactionsCount = await getRepository(Transaction).count({
          where: [
            {from: user},
            {to: user},
            {createdAt: Between(lastMonth, now)}
          ],
        })  

        // Send email to user 
        await emailService.SendEmail({
          type: 'monthlyStatus',
          to: user.email,
          nickname: user.nickname,
          transactionsCount,
          currentBalance, 
          previousBalance
        });
      } catch (err) {
        console.log(err)
      }

    });
  } catch(err) {
    console.log(err)
    throw new InternalServerErrorException; 
  }
  
  return true;
};

export default AccountStatusCron;
