import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { TransactionWorkerService } from '../../modules/transaction/transaction.worker';
import { UserService } from '../../modules/user/user.service';
import { NotificationService } from '../../modules/notification/notification.service';
import ValidateTotalFunds from './jobs/validateTotalFunds';
import RemoveDraftPayments from './jobs/removeDraftPayments';
import performCosts from './jobs/performCosts';

@Injectable()
export class TasksService {
  // private readonly logger = new Logger(TasksService.name);

  constructor(
    private readonly transactionWorker: TransactionWorkerService,
    private readonly notificationService: NotificationService,
    private readonly userService: UserService,
  ) {}

  // Synchronize user data from auth0 every day
  @Cron('0 3 * * *')
  async handleCron(): Promise<boolean> {
    await this.userService.SyncUSersFromAuth0();
    return true;
  }

  // Check transactionworker every minute
  @Cron('*/1 * * * *')
  pingTransactionWorker(): boolean {
    this.transactionWorker.Ping();
    return true;
  }

  /**
   * 
   * Users get an update by mail if they have new unread email since the last notification they received. 
   * This tasks checks every 30 minutes if there are users with new messages since last time. 
   * The send interval (digest) is a user preference: { immediately, hourly, daily, weekly, never }
   * 
   * if message.read_status = false
   * if message.created_at > now - user.digest
   *
   * */
  @Cron('30 * * * *')
  sendNotifications(): boolean {
    this.notificationService.SendNotifications();
    return true;
  }

  // Validate total funds (daily check)
  @Cron('0 * * * *')
  validateTotalFunds(): boolean {
    return ValidateTotalFunds();
  }

  @Cron('0 0 1 */1 *')
  removeDraftPayments(): boolean {
    return RemoveDraftPayments();
  }

  @Cron('0 10 1 * *')
  performCosts(): boolean {
    return performCosts();
  }
}
