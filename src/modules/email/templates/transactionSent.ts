// module.exports = function(data) {
//   var out = {};

//   out.subject = 'Nieuw tegoed verstuurd (De Haagse Munt)';

//   var html = '';
//   html += 'Hallo ' + data.sender.displayname + ',<br><br>';

//   html += '<p>Je hebt <strong>' + data.transaction.amount + '</strong> HM gestuurd naar <strong>' + data.recipient.displayname  + '</strong></p>';
//   // html += '<p>Je balans is nu '+ data.transaction.balance +' HM.</p><br>';

//   html += '<p><strong>Transactie details:</strong></p>';
//   html += '<table style="text-align:left">';
//   html += '<tr><td><strong>Van: </strong></td><td>' + data.sender.displayname + '</td></tr>';
//   html += '<tr><td><strong>Aan: </strong></td><td>' + data.recipient.displayname + '</td></tr>';
//   html += '<tr><td><strong>Bedrag: </strong></td><td>' + data.transaction.amount + ' HM</td></tr>';
//   if(data.transaction.description) {
//     html += '<tr><td><strong>Beschrijving: </strong></td><td>' + data.transaction.description + '</td></tr>';
//   }
//   html += '</table>';
//   html += '<br>';

//   html += '<p>Bekijk het overzicht van je rekening in je <a href="https://dehaagsemunt.nl/dashboard">dashboard</a>.</p><br></br>';

//   html += '<p>De Haagse Munt</p><br><br>';

//   out.html = html;

//   // Plain message
//   let plain = '';
//   plain += 'Hallo ' + data.sender.displayname + ',\n';

//   plain += 'Je hebt nieuw tegoed gestuurd naar ' + data.recipient.displayname  + ' van je rekening bij De Haagse Munt: \n';
//   plain += 'Aantal: ' + data.transaction.amount + ' HM \n\n';
//   plain += 'Transactie details: \n';
//   plain += 'Van: ' + data.sender.displayname + '\n';
//   plain += 'Aan: </strong></td><td>' + data.recipient.displayname + ' \n';
//   plain += 'Bedrag: </strong></td><td>' + data.transaction.amount + ' HM \n';
//   plain += 'Beschrijving: </strong></td><td>' + data.transaction.description + ' \n';
//   plain += '\n\n';

//   plain += 'Bekijk het overzicht van je rekening op https://dehaagsemunt.nl/dashboard. \n\n\n';

//   plain += 'De Haagse Munt';

//   out.plain = plain;

//   return out;
// };
// // 