import { Module } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { MessageModule } from './../message/message.module';
import { UserModule } from '../user/user.module';
import { EmailModule } from '../email/email.module';

@Module({
  imports: [MessageModule, UserModule, EmailModule],
  providers: [NotificationService],
  exports: [NotificationService],
})
export class NotificationsModule {}
