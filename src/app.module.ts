import { Module } from '@nestjs/common';
import * as Joi from 'joi';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { AppController } from './app.controller';
import { DatabaseModules } from './modules/database/database.module';
import { UserModule } from './modules/user/user.module';
import { AuthzModule } from './modules/authz/authz.module';
import { BalanceModule } from './modules/balance/balance.module';
import { AdminModule } from './modules/admin/admin.module';
import { MessageModule } from './modules/message/message.module';
import { PaymentModule } from './modules/payment/payment.module';
import { TransactionModule } from './modules/transaction/transaction.module';
import { VoucherModule } from './modules/voucher/voucher.module';
import { ScheduleModule } from '@nestjs/schedule';
import { TasksModule } from './modules/tasks/tasks.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        DB_HOST: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_USER: Joi.string().required(),
        DB_PASSWORD: Joi.string().required(),
        DB_DATABASE: Joi.string().required(),
        DB_SYNC: Joi.boolean().required(),
        DB_LOG: Joi.boolean().required(),

        PORT: Joi.number(),
      }),
    }),
    DatabaseModules,
    UserModule,
    AuthzModule,

    BalanceModule,
    AdminModule,
    MessageModule,
    PaymentModule,
    TransactionModule,
    VoucherModule,
    ScheduleModule.forRoot(),
    TasksModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {
  static port: number | string;
  static isDev: boolean;
  static sentryKey: string;

  constructor(private readonly _configService: ConfigService) {
    AppModule.port = AppModule.normalizePort(_configService.get('PORT'));
    console.log(process.env.NODE_ENV);
    AppModule.isDev = _configService.get('NODE_ENV') == 'development';
    AppModule.sentryKey = _configService.get('SENTRY_DSN');
  }

  /**
   * Normalize port or return an error if port is not valid
   * @param val The port to normalize
   */
  private static normalizePort(val: number | string): number | string {
    const port: number = typeof val === 'string' ? parseInt(val, 10) : val;

    if (Number.isNaN(port)) return val;
    if (port >= 0) return port;

    throw new Error(`Port "${val}" is invalid.`);
  }
}
